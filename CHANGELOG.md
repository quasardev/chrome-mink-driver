Changelog
=========

## 1.0.1

Fixed back() and forward() timing out when the page is served from cache.
